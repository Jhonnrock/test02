const express = require("express");
const app = express();
app.get('/', (req, res) => {
    res.sendfile("index.html")
});
app.use(express.static(__dirname + "app/"));
app.listen(3500, () => {
    console.log("listener en el puerto 3500");
});